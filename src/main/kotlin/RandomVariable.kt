class RandomVariable(val values: List<Value>) {
    val size: Int
        get() = values.size

    val n: Int
        get() = values.map { it.count }.sum()

    val E: Double
        get() = values.map { it.value * it.count / n }.sum()

    val Var: Double
        get() = pow(2.0).E - Math.pow(E, 2.0)

    val s2n: Double
        get() {
            val Xn = E;
            return values.map { Math.pow(it.value - Xn, 2.0) * it.count }.sum() / (n -1)
        }

    fun pow(exponent: Double): RandomVariable {
        return RandomVariable(values.map { Value(Math.pow(it.value, exponent), it.count) })
    }

    fun valuesToString(formatter: (Double) -> String): String {
        return "\"values\": {\n" + values.sortedBy { it.value }.map { "\t\"${formatter(it.value)}\": ${it.count}" }.fold("", {a, b -> "$a,\n$b"}) + "\n}"
    }

    fun toString(formatter: (Double) -> String): String {
        return "RandomVariable{\"n\": $n, \"size\": $size, \"E\": $E, \"var\": $Var,\"s2n\": $s2n\n${valuesToString(formatter)}"
    }

    override fun toString() = toString(Double::toString)
    operator fun get(index: Int): Value {
        return values[index];
    }
}

data class Value(val value: Double, val count: Int)
