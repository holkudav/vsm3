import java.io.File


fun main(args: Array<String>) {
    val charDistribution1 = createCharRandomVariable(args[0])
    val charDistribution2 = createCharRandomVariable(args[1])

    println("Rozdeleni pismen v 1. textu = size: ${charDistribution1.size}, values: ${charDistribution1.valuesToString { it.toInt().toChar().toString() }}")
    println("Rozdeleni pismen v 2. textu = size: ${charDistribution2.size}, values: ${charDistribution2.valuesToString { it.toInt().toChar().toString() }}")

    val alphabet = charDistribution1.values.mapIndexed { i, it -> Pair(it.value.toInt().toChar(), i) }.toMap()
    val P = changes(args[0].getText(), alphabet).map { row ->
        row.map { it / row.sum().toDouble() }
    }

    println("Matice P," + alphabet.keys.toList().joinToString())
    P.forEachIndexed { i, it ->
        println("${alphabet.keys.toList()[i]}, ${it.joinToString()}")
    }

    val P100 = matrixOnPower(P, 100)
//    println("Matice P^100," + alphabet.keys.toList().joinToString())
//    P100.forEachIndexed { i, it ->
//        println("${alphabet.keys.toList()[i]}, ${it.joinToString()}")
//    }

    val pi = P100[0];
    println("pi = $pi")
    val zkouska = multiplyMatrices(listOf(pi), P, 1, 27, 27)
    println("pi*P = $zkouska")

    println("Pocet znaku v 2. textu ${charDistribution2.n}")
    val z = charDistribution2.values.last()
    println("z = $z")
    val distribution = charDistribution2.values.map {
        if (it.value.toInt().toChar() == 'q') {
            Value(it.value, it.count + z.count)
        } else {
            it
        }
    }.dropLast(1)

    val piAdjusted = pi.mapIndexed { i, it ->
        if (i == 17) {
            it + pi.last()
        } else {
            it
        }
    }.dropLast(1)

    println("Pi adjusted: $piAdjusted")

    println("Hodnota testovane statistiky X^2 pri znamych parametrech: ${chikvadrat(distribution, piAdjusted, charDistribution2.n)}")
}

fun createCharRandomVariable(fileName: String): RandomVariable =
    RandomVariable(fileName
        .getText()
        .fold(emptyMap(), { acc: Map<Char, Int>, c: Char ->
            acc + Pair(c, acc.getOrDefault(c, 0) + 1)
        })
        .map { Value(it.key.toInt().toDouble(), it.value) }
        .sortedBy { it.value }
    )

fun String.getText(): String = File(this)
    .readText(Charsets.UTF_8)
    .split("\n")[1]
    .map { if (it == ' ') '_' else it }
    .joinToString("")

fun changes (text: String, alphabet: Map<Char, Int>): List<List<Int>> {
    val result = arrayListOf<ArrayList<Int>>()
    alphabet.forEach {
        val inner = arrayListOf<Int>()
        alphabet.forEach {
            inner += 0
        }
        result += inner
    }

    text.substring(1).fold(text[0],  { p, c ->
        val previous = alphabet[p]!!
        val current = alphabet[c]!!

        val temp: Int = result[previous][current] + 1
        result[previous][current] = temp
        c
    })

    return result
}

fun matrixOnPower(matrix: List<List<Double>>, exponent: Int): List<List<Double>> {
    val size = matrix.size

    return (0 .. exponent).fold(matrix, { acc, it ->
        multiplyMatrices(acc, matrix, size, size, size)
    })
}

fun multiplyMatrices(firstMatrix: List <List<Double>>,
                     secondMatrix: List <List<Double>>,
                     r1: Int,
                     c1: Int,
                     c2: Int): List <List<Double>> {
    val product = Array(r1) { DoubleArray(c2) }
    for (i in 0 until r1) {
        for (j in 0 until c2) {
            for (k in 0 until c1) {
                product[i][j] += firstMatrix[i][k] * secondMatrix[k][j]
            }
        }
    }

    return product.map { it.toList() }
}

fun chikvadrat(distribution: List<Value>, pi: List<Double>, n: Int): Double {
    return distribution.mapIndexed { i, it ->
        val npi: Double = n * pi[i]
        Math.pow(it.count - npi, 2.0) / npi
    }.sum()
}